public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        Car car1  = new Car();
        //System.out.println(car1.brand);
        //System.out.println(car1.make);
        //System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 2000000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
        //System.out.println(car1);

        //Instance - an object created from a class and each instance of a class should be independent to one another
        Car car2 = new Car();
        //System.out.println(car2.brand);
//        car2.make = "Tamaraw FX";
//        car2.brand = "Toyota";
//        car2.price = 450000;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
        //We cannot add value to a property that does not exist or defined in our class.
        //car2.driver = "Alejandro";

        /*Mini-Activity*/
        Car car3 = new Car();
//        car3.make = "Model X";
//        car3.brand = "Tesla";
//        car3.price = 6000000;
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);

        Car car4 = new Car();
//        System.out.println(car4.brand);

        Driver driver1 = new Driver("Alejandro", 25);

        Car car5 = new Car("Vios", "Toyota", 1500000, driver1);
//        System.out.println(car5.brand);
//        System.out.println(car5.make);
//        System.out.println(car5.price);

        car1.start();
        car2.start();
        car3.start();
        car4.start();
        car5.start();

        //make property getters
        System.out.println(car1.getMake());
        System.out.println(car5.getMake());

        //make property setter
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car5.setMake("Innova");
        System.out.println(car5.getMake());

        /*Mini Activity*/
        car1.setBrand("Bugatti");
        car1.setPrice(2000000);
        System.out.println(car1.getBrand());
        System.out.println(car1.getPrice());

        //carDriver getter
        System.out.println(car5.getCarDriver().getName());

        //carDriver setter
        Driver newDriver = new Driver("Antonio", 21);
        car5.setCarDriver(newDriver);

        System.out.println(car5.getCarDriver().getName());
        System.out.println(car5.getCarDriver().getAge());

        //custom carDriverName
        System.out.println(car5.getCarDriverName());

        //s04 Activity
        Animal animal1 = new Animal("Lily", "Black");

        animal1.call();
        //end

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Malakas");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("White");
        System.out.println(dog1.getColor());

        Dog dog2 = new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();
    }
}